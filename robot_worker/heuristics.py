

def misplaced_with_robot(state):
    score = 0
    for row_g, row_t in zip(state.grid, state.target_grid):
        for cell_g, cell_t in zip(row_g, row_t):
            for item in cell_g[1]:
                if item in cell_t[1]:
                    score += 1
    if state.robot.location == state.target_robot:
        score += 1
    return -score

def misplaced_without_robot(state):
    score = 0
    for row_g, row_t in zip(state.grid, state.target_grid):
        for cell_g, cell_t in zip(row_g, row_t):
            for item in cell_g[1]:
                if item in cell_t[1]:
                    score += 1
    return -score

# calculates manhattan distance taking into account robot's position
# however, I expect the robot not be happy about moving from original position, so 
# I adjusted the score(making items be more important than the robot)
def manahttan_with_robot(state):
    penalty_score = 0
    robot_score = 1
    item_score = 10

    for row_num, row in enumerate(state.grid):
        for col_num, cell in enumerate(row):
            for item in cell[1]:
                target_location = state.target_item_locations[item[0]]
                penalty_score += item_score * ( abs(target_location[0] - row_num) + abs(target_location[1] - col_num))
    robot_location = state.robot.location 
    for item in state.robot.carried_items:
        target_location = state.target_item_locations[item[0]]
        penalty_score += abs(target_location[0] - robot_location[0]) + abs(target_location[1] - robot_location[1])

    penalty_score += robot_score * ( abs(state.robot.location[0] - state.target_robot[0]) + abs(state.robot.location[1] - state.target_robot[1]) )
    return penalty_score

def manhattan_without_robot(state):
    penalty_score = 0

    for row_num, row in enumerate(state.grid):
        for col_num, cell in enumerate(row):
            for item in cell[1]:
                target_location = state.target_item_locations[item[0]]
                penalty_score += abs(target_location[0] - row_num) + abs(target_location[1] - col_num)
    
    robot_location = state.robot.location 
    for item in state.robot.carried_items:
        target_location = state.target_item_locations[item[0]]
        penalty_score += abs(target_location[0] - robot_location[0]) + abs(target_location[1] - robot_location[1])

    return penalty_score
    