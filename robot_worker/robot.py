from copy import deepcopy, copy
from bbmodcache.bbSearch import SearchProblem


class Robot:
    def __init__(self, location, carried_items, strength):
        # tuple(x, y). Robot needs to return to its place at the end
        self.location = location
        # list of instances of an tuple(name, weight)
        self.carried_items = carried_items
        # integer
        self.strength = strength

    def move_possibilities(self):
        # first list is move in x direction
        # second list is move in y direction
        # pair([0][i], [1][i]) represents possible movement options 
        return [
            [1, -1, 0, 0],
            [0, 0, 1, -1]
        ]

    def weight_carried(self):
        return sum([i[1] for i in self.carried_items])

    def __repr__(self):
        return f'<Robot {self.location}: {self.strength}, {self.carried_items}>' 
        

class State:
    def __init__(self, robot, grid, target_grid, target_robot, target_item_locations):
        # robot contains the info about robot's state
        self.robot = robot
        # grid contains the info about robot's state and items' states
        self.grid = grid
        self.target_grid = target_grid
        self.target_robot = target_robot
        self.target_item_locations = target_item_locations
        # together we have a full information about the state
        # grid = 2d list of "cells"
        # each cell is a tuple (blocked(bool), [items])
        # each item = tuple(name, weight)
    def __repr__(self):
        return f'<State: {self.robot} {self.grid}>'

def available(grid, x, y):
    if x < 0 or y < 0:
        return False
    if x >= len(grid[0]) or y >= len(grid):
        return False
    return grid[x][y][0] == False

class RobotWorker(SearchProblem):
    def __init__(self, state, goal_grid):
        # State class instance
        self.initial_state = state
        # Grid instance = goal state
        self.goal_grid = goal_grid

    def possible_actions(self, state):
        robot_location = state.robot.location
        strength = state.robot.strength
        weight_carried = state.robot.weight_carried()

        actions = []
        # Can put down any carried item
        for item in state.robot.carried_items:
            actions.append(("put down", item))

        # Can pick up any item in room if strong enough
        for item in state.grid[robot_location[0]][robot_location[1]][1]:
            if strength >= weight_carried + item[1]:
                actions.append(("pick up", item))

        # robot can move
        for i in range(len(state.robot.move_possibilities()[0])):
            new_location = (robot_location[0] + state.robot.move_possibilities()[0][i], robot_location[1] + state.robot.move_possibilities()[1][i])
            if available(state.grid, new_location[0], new_location[1]):
                actions.append(("move to", new_location))
        # Now the actions list should contain all possible actions
        return actions

    def successor(self, state, action):
        next_state = deepcopy(state)

        act, target = action
        if act == "put down":
            next_state.robot.carried_items.remove(target)
            next_state.grid[state.robot.location[0]][state.robot.location[1]][1].append(target)

        if act == "pick up":
            next_state.grid[state.robot.location[0]][state.robot.location[1]][1].remove(target)
            next_state.robot.carried_items.append(target)

        if act == "move to":
            next_state.robot.location = target
        return next_state


    def goal_test(self, state):
        for row_r, row_g in zip(state.grid, self.goal_grid):
            for cell_r, cell_g in zip(row_r, row_g):
                if len(cell_r[1]) != len(cell_g[1]):
                    return False
                for item_r, item_g in zip(cell_r[1], cell_g[1]):
                    if item_r != item_g:
                        return False
        return state.robot.location == self.initial_state.robot.location

    def display_state(self, state):
        print("Robot location:", state.robot.location)
        print("Robot carrying:", state.robot.carried_items)
        print("Room contents:", state.grid)
