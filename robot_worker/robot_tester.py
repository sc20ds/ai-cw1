from bbmodcache.bbSearch import search
from robot import Robot, State, RobotWorker

from heuristics import manahttan_with_robot, manhattan_without_robot, misplaced_with_robot, misplaced_without_robot






robot_location = (0, 0)
robot_strength = 10

rob = Robot(
    location = robot_location, 
    carried_items = [], 
    strength = robot_strength
)

# example grid 
#  r   _  (1)
#  _  [1]  #
# (2) [2]  #
# Where (x) represents items on that place
# [x] represents the goal of those items
# _ empty field
# # blocked field
# r robot

s = [
    [(0, []), (0, []), (0, [("1", 1)])],
    [(0, []), (0, []), (1, [])],
    [(0, [("2", 2)]), (0, []), (1, [])]
]

goal_grid = [
    [(0, []), (0, []), (0, [])],
    [(0, []), (0, [("1", 1)]), (1, [])],
    [(0, []), (0, [("2", 2)]), (1, [])]
]
state = State(
    robot = rob, 
    grid = s, 
    target_grid = goal_grid, 
    target_robot = robot_location,
    target_item_locations = {
        "1": (1, 1),
        "2": (2, 1)
    }
)




RW_PROBLEM_1 = RobotWorker(
    state,
    goal_grid
)


poss_acts = RW_PROBLEM_1.possible_actions(RW_PROBLEM_1.initial_state)


search( RW_PROBLEM_1, 'bf', 100000, loop_check = True, heuristic = manhattan_without_robot)
