from copy import deepcopy
from bbmodcache.bbSearch import SearchProblem


class Robot:
    def __init__(self, location, carried_items, strength):
        # tuple(x, y). Robot needs to return to its place at the end
        self.location = location
        # list of instances of an Item class
        self.carried_items = carried_items
        # integer
        self.strength = strength

    def move_possibilities(self):
        # first list is move in x direction
        # second list is move in y direction
        # pair([0][i], [1][i]) represents possible movement options 
        return [
            [1, -1, 0, 0],
            [0, 0, 1, -1]
        ]

    def weight_carried(self):
        return sum([i.weight for i in self.carried_items])

    def __repr__(self):
        return f'<Robot {self.location}: {self.strength}, {self.carried_items}>' 
        

class State:
    def __init__(self, robot, grid):
        # robot contains the info about robot's state
        self.robot = robot
        # grid contains the info about robot's state and items' states
        self.grid = grid
        # together we have a full information about the state

    def __repr__(self):
        return f'<State: {self.robot} {self.grid}>'

class Item:
    def __init__(self, name, weight):
        self.name = name
        self.weight = weight
    

class GridCell:
    def __init__(self, blocked, items):
        # can be blocked (unaccessible by the robot)
        self.blocked = blocked
        # list of tuples(name, weight) instances
        self.items = items

class Grid:
    def __init__(self, length, height, grid):
        # length = x-direction
        self.length = length
        # height = y-direction
        self.height = height
        # 2d list of GridCell instances
        self.grid = grid

    # checks if a GridCell is accessible
    def available(self, x, y):
        if x < 0 or y < 0:
            return False
        if x >= self.length or y >= self.height:
            return False
        return self.grid[x][y].blocked == False

class RobotWorker(SearchProblem):
    def __init__(self, state, goal_grid):
        # State class instance
        self.initial_state = state
        # Grid instance = goal state
        self.goal_grid = goal_grid

    def possible_actions(self, state):
        robot_location = state.robot.location
        strength = state.robot.strength
        weight_carried = state.robot.weight_carried()

        actions = []
        # Can put down any carried item
        for item in state.robot.carried_items:
            actions.append(("put down", item))

        # Can pick up any item in room if strong enough
        for item in state.grid.grid[robot_location[0]][robot_location[1]].items:
            if strength >= weight_carried + item.weight:
                actions.append(("pick up", item))

        # robot can move
        for i in range(len(state.robot.move_possibilities())):
            new_location = (robot_location[0] + state.robot.move_possibilities()[0][i], robot_location[1] + state.robot.move_possibilities()[1][i])
            if state.grid.available(new_location[0], new_location[1]):
                actions.append(("move to", new_location))
        # Now the actions list should contain all possible actions
        return actions

    def successor(self, state, action):
        next_state = deepcopy(state)
        act, target = action
        if act == "put down":
            for item in next_state.robot.carried_items:
                if item.name == target.name:
                    next_state.robot.carried_items.remove(item)
            next_state.grid.grid[state.robot.location[0]][state.robot.location[1]].items.append(item)

        if act == "pick up":
            for item in next_state.grid.grid[state.robot.location[0]][state.robot.location[1]].items:
                if item.name == target.name:
                    next_state.grid.grid[state.robot.location[0]][state.robot.location[1]].items.remove(item)
            next_state.robot.carried_items.append(item)

        if act == "move to":
            next_state.robot.location = target
        print("==============================")
        print(next_state.robot)
        for i in range(len(next_state.grid)):
            print(i)
        print("==============================")
        return next_state

    def goal_test(self, state):
        for row_r, row_g in zip(state.grid.grid, self.goal_grid):
            for cell_r, cell_g in zip(row_r, row_g):
                if len(cell_r.items) != len(cell_g.items):
                    return False
                for item_r, item_g in zip(cell_r.items, cell_g.items):
                    if item_r.name != item_g.name or item_r.weight != item_g.name:
                        return False
        return state.robot.location == self.initial_state.robot.location

    def display_state(self, state):
        print("Robot location:", state.robot.location)
        print("Robot carrying:", state.robot.carried_items)
        print("Room contents:", state.grid)
