from copy import deepcopy
from random import choices, randint, sample, seed

from sqlalchemy import true
from bbmodcache.bbSearch import search
from heuristics import manahttan_with_robot, manhattan_without_robot, misplaced_with_robot, misplaced_without_robot
from datetime import datetime

from robot import Robot, RobotWorker, State

def get_items(number_of_items, avg_weight):
    items = []
    for i in range(number_of_items):
        items.append((
            str(i),
            randint(avg_weight // 2, 2*avg_weight)
        ))
    return [
        items,
        sum([item[1] for item in items])
    ]


def find_available(grid, location, available, gridsize):
    if location in available:
        return
    movements = [
        [1, -1, 0, 0],
        [0, 0, 1, -1]
    ]

    for i in range(len(movements[0])):
        next_location = (location[0] + movements[0][i], location[1] + movements[1][i]) 
        if next_location[0] < 0 or next_location[1] < 0:
            continue
        if next_location[0] >= gridsize or next_location[1] >= gridsize:
            continue
        if grid[next_location[0]][next_location[1]][0] == 1:
            continue
        available.append(next_location)
        find_available(grid, next_location, available, gridsize)

def fill_grid(grid, available_cells, robot_start = (0, 0), gridsize = 3, cells = 0):
    points = [
        (i, j) for i in range(gridsize) for j in range(gridsize) 
    ]
    
    for point in sample(points, cells):
        grid[point[0]][point[1]] = (1, [])
    grid[robot_start[0]][robot_start[1]] = (0, [])
    find_available(grid, robot_start, available_cells, gridsize)

def generate_grid(size, blocked = '0', items = []):
    grid = [
        [
            (0, []) for i in range(size)
        ] for i in range(size)
    ]

    if blocked == '0':
        blocked_cells = 0        
    elif blocked == 'O(1)':
        blocked_cells = 3
    elif blocked == 'O(n)':
        blocked_cells = size
    elif blocked == '1/2 * n^2':
        blocked_cells = size * size // 2
    elif blocked == '90% * n^2':
        blocked_cells = size * size // 9 * 10 

    # unblocked_cells = size * size - blocked_cells

    robot_location = (
        randint(0, size - 1),
        randint(0, size - 1)
    )
    grid[robot_location[0]][robot_location[1]] = (0, [])
    available_cells = [] 
    fill_grid(grid, available_cells, robot_start = robot_location, gridsize = size, cells = blocked_cells)

    goal_grid = deepcopy(grid)
    seed(datetime.now())
    item_locations = choices(available_cells, k = len(items))
    seed(datetime.now())
    item_locations_goal = choices(available_cells, k = len(items))
    target_item_locations = {}

    for item_location, goal_location, item in zip(item_locations, item_locations_goal, items):
        grid[item_location[0]][item_location[1]][1].append(item)
        goal_grid[goal_location[0]][goal_location[1]][1].append(item)
        target_item_locations[item[0]] = goal_location

    return grid, goal_grid, robot_location, target_item_locations

def small_test():
    n = [
        3, 
        5,
        10
    ]
    blocked = [
        '0',
        'O(1)',
        'O(n)',
        '1/2 * n^2'
    ]

    k = [
        'O(1)',
        'O(n)'
    ]

    strength = [
        'item',
        'all_items'
    ]
    results = {
        size: {
            b: {
                items_comp: {
                    s: [] for s in strength 
                } for items_comp in k 
            } for b in blocked
        } for size in n
    }

    avg_item_weight = 100
        

    for size in n[:1]:
        for b in blocked[:1]:
            for item_comp in k[:1]:
                for s in strength:
                    for i in range(5):
                        if item_comp == 'O(1)':
                            number_of_items = 3
                        elif item_comp == 'O(n)':
                            number_of_items = n
                        elif item_comp == 'O(n^2)':
                            number_of_items = n * n // 3
                        
                        items, total_weight = get_items(number_of_items, avg_item_weight)
                        if strength == 'item':
                            robot_strength = avg_item_weight * 2 + 1
                        else:
                            robot_strength = total_weight / 2
                        
                        grid, goal_grid, robot_location, target_item_locations = generate_grid(size, b, items)

                        rob = Robot(
                            location = robot_location, 
                            carried_items = [], 
                            strength = robot_strength
                        )

                        state = State(
                            robot = rob, 
                            grid = grid, 
                            target_grid = goal_grid, 
                            target_robot = robot_location,
                            target_item_locations = target_item_locations
                        )
                        print("====================")
                        print("--------------------")
                        print('method:', 'best-first')
                        print('size: ', size)
                        print('blocked:', b)
                        print('items', number_of_items)
                        print('strength', robot_strength)

                        def cost(path, state):
                            return len(path)
                        # BFS 
                        # RW_PROBLEM_1 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_1.possible_actions(RW_PROBLEM_1.initial_state)
                        # search( RW_PROBLEM_1, 'bf', 100000, loop_check = True )
                        
                        # # DFS 
                        # RW_PROBLEM_2 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_2.possible_actions(RW_PROBLEM_2.initial_state)
                        # search( RW_PROBLEM_2, 'df', 50000, loop_check = True )
                        # # DFS randomised
                        # RW_PROBLEM_25 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_25.possible_actions(RW_PROBLEM_25.initial_state)
                        # search( RW_PROBLEM_25, 'df', 50000, loop_check = True, randomise = True)
                        
                        # # Best First - heuristic 1
                        # RW_PROBLEM_3 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_3.possible_actions(RW_PROBLEM_3.initial_state)
                        # search( RW_PROBLEM_3, 'df', 100000, loop_check = True, heuristic = manhattan_without_robot)

                        # # Best First - heuristic 2
                        # RW_PROBLEM_4 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_4.possible_actions(RW_PROBLEM_4.initial_state)
                        # search( RW_PROBLEM_4, 'df', 100000, loop_check = True, heuristic = manahttan_with_robot)
                        
                        # # Best First - heuristic 3
                        # RW_PROBLEM_5 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_5.possible_actions(RW_PROBLEM_5.initial_state)
                        # search( RW_PROBLEM_5, 'df', 100000, loop_check = True, heuristic = misplaced_without_robot)
                        
                        # # Best First - heuristic 4
                        # RW_PROBLEM_6 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_6.possible_actions(RW_PROBLEM_6.initial_state)
                        # search( RW_PROBLEM_6, 'df', 100000, loop_check = True, heuristic = misplaced_with_robot)

                        # # A* - heuristic 1
                        # RW_PROBLEM_7 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_7.possible_actions(RW_PROBLEM_7.initial_state)
                        # search( RW_PROBLEM_7, 'bf', 100000, loop_check = True, heuristic = manhattan_without_robot, cost = cost)

                        # # A* - heuristic 2
                        # RW_PROBLEM_8 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_8.possible_actions(RW_PROBLEM_8.initial_state)
                        # search( RW_PROBLEM_8, 'bf', 100000, loop_check = True, heuristic = manahttan_with_robot, cost = cost)
                        
                        # # A* - heuristic 3
                        # RW_PROBLEM_9 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_9.possible_actions(RW_PROBLEM_9.initial_state)
                        # search( RW_PROBLEM_9, 'bf', 100000, loop_check = True, heuristic = misplaced_without_robot, cost = cost)
                        
                        # # A* - heuristic 4
                        # RW_PROBLEM_10 = RobotWorker(
                        #     deepcopy(state),
                        #     deepcopy(goal_grid)
                        # )
                        # poss_acts = RW_PROBLEM_10.possible_actions(RW_PROBLEM_10.initial_state)
                        # search( RW_PROBLEM_10, 'bf', 100000, loop_check = True, heuristic = misplaced_with_robot, cost = cost)

                        # loopcheck = false
                        RW_PROBLEM_11 = RobotWorker(
                            deepcopy(state),
                            deepcopy(goal_grid)
                        )
                        poss_acts = RW_PROBLEM_11.possible_actions(RW_PROBLEM_11.initial_state)
                        search( RW_PROBLEM_11, 'bf', 100000, loop_check = False )
                        
                        # loopcheck = false
                        RW_PROBLEM_12 = RobotWorker(
                            deepcopy(state),
                            deepcopy(goal_grid)
                        )
                        poss_acts = RW_PROBLEM_12.possible_actions(RW_PROBLEM_12.initial_state)
                        search( RW_PROBLEM_12, 'df', 50000, loop_check = False)
                        print("\n\n===========================================================")
                        print("===========================================================")
                        print("===========================================================")
                        print("===========================================================\n\n")





def best1():
    robot_location = (0, 0)
    robot_strength = 10

    rob = Robot(
        location = robot_location, 
        carried_items = [], 
        strength = robot_strength
    )
    
    
    s = [
        [(0, []), (0, [("1", 1)]), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (1, []), (1, []), (1, []), (1, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, []), (0, []), (0, [])],
    ]
    goal_grid = [
        [(0, []), (0, []), (0, []), (0, []), (0, []), (1, []), (0, [("1", 1)])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (0, []), (0, []), (0, []), (1, []), (0, [])],
        [(0, []), (1, []), (1, []), (1, []), (1, []), (1, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, []), (0, []), (0, [])],
    ]


    target_item_locations = {}
    for r in range(len(s)):
        for c in range(len(s[0])):
            for item in s[r][c][1]:
                target_item_locations[item[0]] = (r,c)

    state = State(
        robot = rob, 
        grid = s, 
        target_grid = goal_grid, 
        target_robot = robot_location,
        target_item_locations = target_item_locations
    )

    def cost(path, state):
        return len(path)
    # BFS 
    RW_PROBLEM_1 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_1.possible_actions(RW_PROBLEM_1.initial_state)
    search( RW_PROBLEM_1, 'bf', 100000, loop_check = True )
    
    # DFS 
    RW_PROBLEM_2 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_2.possible_actions(RW_PROBLEM_2.initial_state)
    search( RW_PROBLEM_2, 'df', 50000, loop_check = True )
    # DFS randomised
    RW_PROBLEM_25 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_25.possible_actions(RW_PROBLEM_25.initial_state)
    search( RW_PROBLEM_25, 'df', 50000, loop_check = True, randomise = True)
    
    # Best First - heuristic 1
    RW_PROBLEM_3 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_3.possible_actions(RW_PROBLEM_3.initial_state)
    search( RW_PROBLEM_3, 'df', 100000, loop_check = True, heuristic = manhattan_without_robot)

    # Best First - heuristic 2
    RW_PROBLEM_4 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_4.possible_actions(RW_PROBLEM_4.initial_state)
    search( RW_PROBLEM_4, 'df', 100000, loop_check = True, heuristic = manahttan_with_robot)
    
    # Best First - heuristic 3
    RW_PROBLEM_5 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_5.possible_actions(RW_PROBLEM_5.initial_state)
    search( RW_PROBLEM_5, 'df', 100000, loop_check = True, heuristic = misplaced_without_robot)
    
    # Best First - heuristic 4
    RW_PROBLEM_6 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_6.possible_actions(RW_PROBLEM_6.initial_state)
    search( RW_PROBLEM_6, 'df', 100000, loop_check = True, heuristic = misplaced_with_robot)

    # A* - heuristic 1
    RW_PROBLEM_7 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_7.possible_actions(RW_PROBLEM_7.initial_state)
    search( RW_PROBLEM_7, 'bf', 100000, loop_check = True, heuristic = manhattan_without_robot, cost = cost)

    # A* - heuristic 2
    RW_PROBLEM_8 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_8.possible_actions(RW_PROBLEM_8.initial_state)
    search( RW_PROBLEM_8, 'bf', 100000, loop_check = True, heuristic = manahttan_with_robot, cost = cost)
    
    # A* - heuristic 3
    RW_PROBLEM_9 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_9.possible_actions(RW_PROBLEM_9.initial_state)
    search( RW_PROBLEM_9, 'bf', 100000, loop_check = True, heuristic = misplaced_without_robot, cost = cost)
    
    # A* - heuristic 4
    RW_PROBLEM_10 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_10.possible_actions(RW_PROBLEM_10.initial_state)
    search( RW_PROBLEM_10, 'bf', 100000, loop_check = True, heuristic = misplaced_with_robot, cost = cost)

    # loopcheck = false
    RW_PROBLEM_11 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_11.possible_actions(RW_PROBLEM_11.initial_state)
    search( RW_PROBLEM_11, 'bf', 100000, loop_check = False )
    
    # loopcheck = false
    RW_PROBLEM_12 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_12.possible_actions(RW_PROBLEM_12.initial_state)
    search( RW_PROBLEM_12, 'df', 50000, loop_check = False)


def best2():
    robot_location = (2, 2)
    robot_strength = 10

    rob = Robot(
        location = robot_location, 
        carried_items = [], 
        strength = robot_strength
    )
    
    
    s = [
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, [("1", 1)]), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
    ]
    
    goal_grid = [
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [("1", 1)])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
        [(0, []), (0, []), (0, []), (0, []), (0, [])],
    ]
    


    target_item_locations = {}
    for r in range(len(s)):
        for c in range(len(s[0])):
            for item in s[r][c][1]:
                target_item_locations[item[0]] = (r,c)

    state = State(
        robot = rob, 
        grid = s, 
        target_grid = goal_grid, 
        target_robot = robot_location,
        target_item_locations = target_item_locations
    )

    def cost(path, state):
        return len(path)
    # BFS 
    RW_PROBLEM_1 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_1.possible_actions(RW_PROBLEM_1.initial_state)
    search( RW_PROBLEM_1, 'bf', 100000, loop_check = True )
    
    # DFS 
    RW_PROBLEM_2 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_2.possible_actions(RW_PROBLEM_2.initial_state)
    search( RW_PROBLEM_2, 'df', 50000, loop_check = True )
    # DFS randomised
    RW_PROBLEM_25 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_25.possible_actions(RW_PROBLEM_25.initial_state)
    search( RW_PROBLEM_25, 'df', 50000, loop_check = True, randomise = True)
    
    # Best First - heuristic 1
    RW_PROBLEM_3 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_3.possible_actions(RW_PROBLEM_3.initial_state)
    search( RW_PROBLEM_3, 'df', 100000, loop_check = True, heuristic = manhattan_without_robot)

    # Best First - heuristic 2
    RW_PROBLEM_4 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_4.possible_actions(RW_PROBLEM_4.initial_state)
    search( RW_PROBLEM_4, 'df', 100000, loop_check = True, heuristic = manahttan_with_robot)
    
    # Best First - heuristic 3
    RW_PROBLEM_5 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_5.possible_actions(RW_PROBLEM_5.initial_state)
    search( RW_PROBLEM_5, 'df', 100000, loop_check = True, heuristic = misplaced_without_robot)
    
    # Best First - heuristic 4
    RW_PROBLEM_6 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_6.possible_actions(RW_PROBLEM_6.initial_state)
    search( RW_PROBLEM_6, 'df', 100000, loop_check = True, heuristic = misplaced_with_robot)

    # A* - heuristic 1
    RW_PROBLEM_7 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_7.possible_actions(RW_PROBLEM_7.initial_state)
    search( RW_PROBLEM_7, 'bf', 100000, loop_check = True, heuristic = manhattan_without_robot, cost = cost)

    # A* - heuristic 2
    RW_PROBLEM_8 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_8.possible_actions(RW_PROBLEM_8.initial_state)
    search( RW_PROBLEM_8, 'bf', 100000, loop_check = True, heuristic = manahttan_with_robot, cost = cost)
    
    # A* - heuristic 3
    RW_PROBLEM_9 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_9.possible_actions(RW_PROBLEM_9.initial_state)
    search( RW_PROBLEM_9, 'bf', 100000, loop_check = True, heuristic = misplaced_without_robot, cost = cost)
    
    # A* - heuristic 4
    RW_PROBLEM_10 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_10.possible_actions(RW_PROBLEM_10.initial_state)
    search( RW_PROBLEM_10, 'bf', 100000, loop_check = True, heuristic = misplaced_with_robot, cost = cost)

    # loopcheck = false
    RW_PROBLEM_11 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_11.possible_actions(RW_PROBLEM_11.initial_state)
    search( RW_PROBLEM_11, 'bf', 100000, loop_check = False )
    
    # loopcheck = false
    RW_PROBLEM_12 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_12.possible_actions(RW_PROBLEM_12.initial_state)
    search( RW_PROBLEM_12, 'df', 50000, loop_check = False)
    


def best_first_tester():
    print("BEST 1")
    best1()
    # print("+++++++++++++++++++++++++++++++++++++++++++")
    # print("BEST 2")
    # best2()


def bfs1():
    robot_location = (1, 1)
    robot_strength = 100

    rob = Robot(
        location = robot_location, 
        carried_items = [], 
        strength = robot_strength
    )
    
    
    s = [
        [(0, []), (0, [("1", 1)]), (0, [])],
        [(0, [("4", 4)]), (0, []), (0, [("2", 2)])],
        [(0, []), (0, [("3", 3)]), (0, [])]
    ]
    goal_grid = [
        [(0, [("2", 2)]), (0, []), (0, [("3", 3)])],
        [(0, []), (0, []), (0, [])],
        [(0, [("1", 1)]), (0, []), (0, [("4", 4)])]
    ]



    target_item_locations = {}
    for r in range(len(s)):
        for c in range(len(s[0])):
            for item in s[r][c][1]:
                target_item_locations[item[0]] = (r,c)

    state = State(
        robot = rob, 
        grid = s, 
        target_grid = goal_grid, 
        target_robot = robot_location,
        target_item_locations = target_item_locations
    )
    def cost(path, state):
        return len(path)
    # BFS 
    RW_PROBLEM_1 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_1.possible_actions(RW_PROBLEM_1.initial_state)
    search( RW_PROBLEM_1, 'bf', 100000, loop_check = True )
    
    # DFS 
    RW_PROBLEM_2 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_2.possible_actions(RW_PROBLEM_2.initial_state)
    search( RW_PROBLEM_2, 'df', 50000, loop_check = True )
    # DFS randomised
    RW_PROBLEM_25 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_25.possible_actions(RW_PROBLEM_25.initial_state)
    search( RW_PROBLEM_25, 'df', 50000, loop_check = True, randomise = True)
    
    # Best First - heuristic 1
    RW_PROBLEM_3 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_3.possible_actions(RW_PROBLEM_3.initial_state)
    search( RW_PROBLEM_3, 'df', 100000, loop_check = True, heuristic = manhattan_without_robot)

    # Best First - heuristic 2
    RW_PROBLEM_4 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_4.possible_actions(RW_PROBLEM_4.initial_state)
    search( RW_PROBLEM_4, 'df', 100000, loop_check = True, heuristic = manahttan_with_robot)
    
    # Best First - heuristic 3
    RW_PROBLEM_5 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_5.possible_actions(RW_PROBLEM_5.initial_state)
    search( RW_PROBLEM_5, 'df', 100000, loop_check = True, heuristic = misplaced_without_robot)
    
    # Best First - heuristic 4
    RW_PROBLEM_6 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_6.possible_actions(RW_PROBLEM_6.initial_state)
    search( RW_PROBLEM_6, 'df', 100000, loop_check = True, heuristic = misplaced_with_robot)

    # A* - heuristic 1
    RW_PROBLEM_7 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_7.possible_actions(RW_PROBLEM_7.initial_state)
    search( RW_PROBLEM_7, 'bf', 100000, loop_check = True, heuristic = manhattan_without_robot, cost = cost)

    # A* - heuristic 2
    RW_PROBLEM_8 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_8.possible_actions(RW_PROBLEM_8.initial_state)
    search( RW_PROBLEM_8, 'bf', 100000, loop_check = True, heuristic = manahttan_with_robot, cost = cost)
    
    # A* - heuristic 3
    RW_PROBLEM_9 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_9.possible_actions(RW_PROBLEM_9.initial_state)
    search( RW_PROBLEM_9, 'bf', 100000, loop_check = True, heuristic = misplaced_without_robot, cost = cost)
    
    # A* - heuristic 4
    RW_PROBLEM_10 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_10.possible_actions(RW_PROBLEM_10.initial_state)
    search( RW_PROBLEM_10, 'bf', 100000, loop_check = True, heuristic = misplaced_with_robot, cost = cost)

    # loopcheck = false
    RW_PROBLEM_11 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_11.possible_actions(RW_PROBLEM_11.initial_state)
    search( RW_PROBLEM_11, 'bf', 100000, loop_check = True )
    
    # loopcheck = false
    RW_PROBLEM_12 = RobotWorker(
        deepcopy(state),
        deepcopy(goal_grid)
    )
    poss_acts = RW_PROBLEM_12.possible_actions(RW_PROBLEM_12.initial_state)
    search( RW_PROBLEM_12, 'df', 50000, loop_check = True)


def bfs_tester():
    bfs1()
    # bfs2()