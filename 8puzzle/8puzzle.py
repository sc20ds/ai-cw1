get_ipython().system('mkdir -p bbmodcache')
get_ipython().system('curl http://bb-ai.net.s3.amazonaws.com/bb-python-modules/bbSearch.py > bbmodcache/bbSearch.py')
get_ipython().system('curl http://bb-ai.net.s3.amazonaws.com/bb-python-modules/crazy8heuristics.py > bbmodcache/crazy8heuristics.py')

from bbmodcache.bbSearch import SearchProblem, search
from copy import deepcopy

# # Number position in layout function
# It is helpful in defining the heuristic functions
# It returns a tuple (row, col) of the "n" element in the provided layout.
def number_position_in_layout( n, layout):
    for i, row in enumerate(layout):
        for j, val in enumerate(row):
            if val==n:
                return (i,j)


# # EightPuzzle class problem definition
class EightPuzzle( SearchProblem ):
        
    action_dict = {
        (0,0) : [(1, 0, 'up'),    (0, 1, 'left')],
        (0,1) : [(0, 0, 'right'), (1, 1, 'up'),    (0, 2, 'left')],
        (0,2) : [(0, 1, 'right'), (1, 2, 'up')],
        (1,0) : [(0, 0, 'down'),  (1, 1, 'left'),  (2, 0, 'up')],
        (1,1) : [(1, 0, 'right'), (0, 1, 'down'),  (1, 2, 'left'), (2, 1, 'up')],
        (1,2) : [(0, 2, 'down'),  (1, 1, 'right'), (2, 2, 'up')],
        (2,0) : [(1, 0, 'down'),  (2, 1, 'left')],
        (2,1) : [(2, 0, 'right'), (1, 1, 'down'),  (2, 2, 'left')],
        (2,2) : [(2, 1, 'right'), (1, 2, 'down')]
    }

    def __init__(self, initial_layout, goal_layout ):
        pos0 = number_position_in_layout( 0, initial_layout )
        # Initial state is pair giving initial position of space
        # and the initial tile layout.
        self.initial_state = ( pos0, initial_layout)
        self.goal_layout   = goal_layout

    ### I just use the position on the board (state[0]) to look up the 
    ### appropriate sequence of possible actions.
    def possible_actions( self, state ):
        actions =  EightPuzzle.action_dict[state[0]]
        actions_with_tile_num = []
        for r, c, d in actions:
            tile_num = state[1][r][c] ## find number of moving tile
            # construct the action representation including the tile number
            actions_with_tile_num.append( (r, c, (tile_num,d)) )
        return actions_with_tile_num

    def successor(self, state, action):
        old0row, old0col  =  state[0]    # get start position
        new0row, new0col, move = action  # unpack the action representation
        moving_number, _ = move
        ## Make a copy of the old layout
        newlayout = deepcopy(state[1])
        # Swap the positions of the new number and the space (rep by 0)
        newlayout[old0row][old0col] = moving_number
        newlayout[new0row][new0col] = 0
        return ((new0row, new0col), newlayout )
    
    def goal_test(self,state):
        return state[1] == self.goal_layout
    
    def display_action(self, action):
        _,_, move = action
        tile_num, direction = move
        print("Move tile", tile_num, direction)
        
    def display_state(self, state):
        for row in state[1]:
            nums = [ (n if n>0 else '.') for n in row]
            print( "   ", nums[0], nums[1], nums[2] )
         
        
    # heuristics
    def manhattan(self, state):
        distance = 0
        # state[0] is the position of the empty cell
        # state[1] is an array of arrays with the state's layout
        # self.goal_layout is where we want to get

        for r, row in enumerate(state[1]):
            for c, tile in enumerate(row):
                if tile != 0:
                    # let's get the goal position for this tile
                    goal_r, goal_c = number_position_in_layout( tile, self.goal_layout )

                    distance += abs(r - goal_r) + abs(c - goal_c)
                    
        return distance
    
    def misplaced_tiles(self, state):
        misplaced = 0
        
        for r, row in enumerate(state[1]):
            for c, tile in enumerate(row):
                if tile != 0:
                    if tile != self.goal_layout[r][c]:
                        misplaced += 1
        
        return misplaced
    
    
    # cost function
    def cost(self, path, state):
        # we assume that every action costs one unit of effort therefore it is the len(path)
        return len(path)
        
        


# # Example definition of a problem 
# We use a layout of the puzzle and the desired goal state
# EP = EightPuzzle( LAYOUT, GOAL )

# # Test cases
# We should aim to cover the following types of search algorithms and options, as well as different levels of difficulty of the problem itself:
# We should focus on 2 or 3 cases which clearly show the differences between the algorithms.
# It should be easy, moderate and a hard case which ideally should require a heuristic for a solution to be found.
# This difficulty should be given by the length of path from the initial state to the goal state.

## A couple of possible goal states:
NORMAL_GOAL = [ [1,2,3],
                [4,5,6],
                [7,8,0] ]

SPIRAL_GOAL = [ [1,2,3],
                [8,0,4],
                [7,6,5] ]

### Declarations of some example states
LAYOUT_0 = [ [1,5,2],
             [0,4,3],
             [7,8,6] ]

LAYOUT_1 = [ [5,1,7],
             [2,4,8],
             [6,3,0] ] 

# this is not solvable
LAYOUT_2 = [ [2,6,3],
             [4,0,5],
             [1,8,7] ] 

# this is not solvable
LAYOUT_3 = [ [7,2,5],
             [4,8,1],
             [3,0,6] ] 

LAYOUT_4 = [ [8,6,7],  
             [2,5,4],
             [3,0,1] ] 


# # Checking if a puzzle is solvable
# It is not possible to solve an instance of 8 puzzle if number of inversions is odd in the input state. 
# A pair of tiles form an inversion if the values on tiles are in reverse order of their appearance in goal state.

# A utility function to count
# inversions in given array 'arr[]'
def getInvCount(arr):
    inv_count = 0
    empty_value = 0
    for i in range(0, 9):
        for j in range(i + 1, 9):
            if arr[j] != empty_value and arr[i] != empty_value and arr[i] > arr[j]:
                inv_count += 1
    return inv_count

def isSolvable(puzzle) :
    # Count inversions in given 8 puzzle
    inv_count = getInvCount([j for sub in puzzle for j in sub])

    # return true if inversion count is even.
    return (inv_count % 2 == 0)
    
puzzles = [LAYOUT_0, LAYOUT_1, LAYOUT_2, LAYOUT_3, LAYOUT_4]
for puzzle in puzzles:
    if(isSolvable(puzzle)) :
        print(f'{puzzle} is solvable')
    else :
        print(f'{puzzle} is not solvable')

# define problems with the above layouts
EP_0N = EightPuzzle( LAYOUT_0, NORMAL_GOAL )
EP_1N = EightPuzzle( LAYOUT_1, NORMAL_GOAL )
EP_2N = EightPuzzle( LAYOUT_2, NORMAL_GOAL )
EP_3N = EightPuzzle( LAYOUT_3, NORMAL_GOAL )
EP_4N = EightPuzzle( LAYOUT_4, NORMAL_GOAL )

# I didn't really tested these yet
EP_0S = EightPuzzle( LAYOUT_0, SPIRAL_GOAL )
EP_1S = EightPuzzle( LAYOUT_1, SPIRAL_GOAL )
EP_2S = EightPuzzle( LAYOUT_2, SPIRAL_GOAL )
EP_3S = EightPuzzle( LAYOUT_3, SPIRAL_GOAL )
EP_4S = EightPuzzle( LAYOUT_4, SPIRAL_GOAL )

# Breadth-first search (BFS)
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=True)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 1000000, loop_check=True)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 1000000, loop_check=True)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 1000000, loop_check=True)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 1000000, loop_check=True)


# Breadth-first search (BFS)
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=False)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 1000000, loop_check=False)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 1000000, loop_check=False)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 1000000, loop_check=False)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 1000000, loop_check=False)


# Depth-first search (DFS)
# with fixed action choice ordering
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'DF/LIFO', 10000, loop_check=True, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_1N, 'DF/LIFO', 50000, loop_check=True, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_2N, 'DF/LIFO', 100000, loop_check=True, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_3N, 'DF/LIFO', 100000, loop_check=True, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_4N, 'DF/LIFO', 50000, loop_check=True, randomise=False, show_path=False)


# Depth-first search (DFS)
# with fixed action choice ordering
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'DF/LIFO', 100000, loop_check=False, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_1N, 'DF/LIFO', 100000, loop_check=False, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_2N, 'DF/LIFO', 100000, loop_check=False, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_3N, 'DF/LIFO', 100000, loop_check=False, randomise=False, show_path=False)

print('\n--------------------------\n')
search(EP_4N, 'DF/LIFO', 50000, loop_check=False, randomise=False, show_path=False)


# Depth-first search (DFS)
# with randomised action choice ordering
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_1N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_2N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_3N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_4N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)


# Depth-first search (DFS)
# with randomised action choice ordering
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_1N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_2N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_3N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)

print('\n--------------------------\n')
search(EP_4N, 'DF/LIFO', 100000, loop_check=True, randomise=True, show_path=False)


# Best-first (BFS with heuristic)
# manhattan distance
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=True, heuristic=EP_0N.manhattan)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_1N.manhattan)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_2N.manhattan)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_3N.manhattan)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_4N.manhattan)


# Best-first (BFS with heuristic)
# manhattan distance
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=False, heuristic=EP_0N.manhattan)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_1N.manhattan)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_2N.manhattan)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_3N.manhattan)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_4N.manhattan)


# Best-first (BFS with heuristic)
# misplaced tiles 
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=True, heuristic=EP_0N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_1N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_2N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_3N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_4N.misplaced_tiles)


# Best-first (BFS with heuristic)
# misplaced tiles 
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=False, heuristic=EP_0N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_1N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_2N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_3N.misplaced_tiles)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_4N.misplaced_tiles)


# A* (BFS with heuristic and a cost function)
# manhattan distance heuristic
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=True, heuristic=EP_0N.manhattan, cost=EP_0N.cost)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_1N.manhattan, cost=EP_1N.cost)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_2N.manhattan, cost=EP_2N.cost)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_3N.manhattan, cost=EP_3N.cost)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_4N.manhattan, cost=EP_4N.cost)


# A* (BFS with heuristic and a cost function)
# manhattan distance heuristic
## loop checking false

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=False, heuristic=EP_0N.manhattan, cost=EP_0N.cost)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_1N.manhattan, cost=EP_1N.cost)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_2N.manhattan, cost=EP_2N.cost)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_3N.manhattan, cost=EP_3N.cost)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_4N.manhattan, cost=EP_4N.cost)


# A* (BFS with heuristic and a cost function)
# misplaced tiles heuristic
## loop checking true

print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=True, heuristic=EP_0N.misplaced_tiles, cost=EP_0N.cost)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_1N.misplaced_tiles, cost=EP_1N.cost)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_2N.misplaced_tiles, cost=EP_2N.cost)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_3N.misplaced_tiles, cost=EP_3N.cost)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=True, heuristic=EP_4N.misplaced_tiles, cost=EP_4N.cost)


# A* (BFS with heuristic and a cost function)
# misplaced tiles heuristic
## loop checking false


print('\n--------------------------\n')
search(EP_0N, 'BF/FIFO', 5000, loop_check=False, heuristic=EP_0N.misplaced_tiles, cost=EP_0N.cost)

print('\n--------------------------\n')
search(EP_1N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_1N.misplaced_tiles, cost=EP_1N.cost)

print('\n--------------------------\n')
search(EP_2N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_2N.misplaced_tiles, cost=EP_2N.cost)

print('\n--------------------------\n')
search(EP_3N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_3N.misplaced_tiles, cost=EP_3N.cost)

print('\n--------------------------\n')
search(EP_4N, 'BF/FIFO', 100000, loop_check=False, heuristic=EP_4N.misplaced_tiles, cost=EP_4N.cost)


